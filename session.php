<?php
    
if(!isset($_SESSION)) session_start();  //must be in the code of every session related task


if(isset($_SESSION['userName'])){

    echo "From session: " . $_SESSION['userName']."<br>";
}

elseif(isset($_POST['userName'])){
    echo  "From POST: " .$_POST['userName']."<br>";
    echo  "From REQUEST: " .$_REQUEST['userName']."<br>";

    $_SESSION['userName']=  $_POST['userName'];    
}

else{
    header('Location: anyForm.html');
}

?>

<a href="logout.php">LogOut</a>
